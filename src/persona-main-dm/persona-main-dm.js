import {LitElement, html} from 'lit';

class PersonaMainDM extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            people: {type: Array}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
        this.people = [
            {
                name: "Manolo",
                yearsInCompany: 10,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Manolo"
                },
                profile: "Jefe de equipo"
            }, {            
                name: "Josefa",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Josefa"
                }
                ,
                profile: "Programador Junior"
            }, {
                name: "MariTere",
                yearsInCompany: 5,
                photo: {
                    src: "./img/persona.gif",
                    alt: "MariTere"
                },
                profile: "Programador Senior"
            }, {
                name: "Ataulfo",
                yearsInCompany: 8,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Ataulfo"
                },
                profile: "Analista"
            }, {
                name: "Joshua",
                yearsInCompany: 7,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Joshua"
                },
                profile: "Analista/Programador"
            }, {
                name: "Estefany",
                yearsInCompany: 1,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Estefany"
                },
                profile: "Programador Junior"
            }
        ];
        this.showPersonForm = false;
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){
        console.log("updated en persona-main-DM");
        this.dispatchEvent(new CustomEvent("dataManagerPeople",
        {
            detail: {
                personas : this.people
            }
        }));

    }
}

customElements.define("persona-main-dm",PersonaMainDM);

// Aserción
// mocha.AssertEquals()calculadora.suma(2,2), 4);
// Invariante = 4 (pasado en la función Assert)

// Actual VS Expected y Fluent Interfaces
// mocha.should.be.equal(4).calculadora.suma(2,2);

// Ejemplo Mocks
// Conversor Moneda
// calculator = XXnewCalculatorXX -> calculatorMock <- (herramienta de Mocks) Se llama a un código 
// currencyConverter.convert("USD","EUR", calculator.mult(2, EUR.value))