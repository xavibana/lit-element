import {LitElement, html} from 'lit';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <h1>Gestor Evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e){
        console.log("Capturando evento del emisor");
        console.log(e);

        this.shadowRoot.getElementById("receiver").year = e.detail.year;
        this.shadowRoot.getElementById("receiver").course = e.detail.course;

    }


}

customElements.define("gestor-evento",GestorEvento);