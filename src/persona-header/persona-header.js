import {LitElement, html} from 'lit';

class PersonaHeader extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <h1>Persona Header</h1>
        `;
    }


}

customElements.define("persona-header",PersonaHeader);