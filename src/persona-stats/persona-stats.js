import {LitElement, html} from 'lit';

class PersonaStats extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            people: {type: Array}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
        this.people = [];
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){
        console.log("updated en persona-stats");

        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-stats");

            let peopleStats = this.gatherPeopleArrayInfo(this.people);

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people-stats",
                    {
                        detail: {
                            peopleStats: peopleStats
                        }
                    }
                )
            )
        }
    }

    gatherPeopleArrayInfo(people){
        console.log("gatherPeopleArrayInfo");
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        console.log(peopleStats);
        return peopleStats;
    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <h1>Persona Stats</h1>
        `;
    }


}

customElements.define("persona-stats",PersonaStats);