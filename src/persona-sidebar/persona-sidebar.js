import {LitElement, html} from 'lit';

class PersonaSidebar extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            peopleStats: {type: Object}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
        this.peopleStats = {};
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <aside>
            <section>
                <div class="mt-5">
                    <button class="btn btn-success w-100"
                    @click="${this.newPerson}"><strong style="font-size: 50px">+</strong></button>
                </div>
                <div>
                    Hay <span>${this.peopleStats.numberOfPeople}</span> personas
                </div>
            </section>
        </aside>
        `;
    }

    newPerson(e){
        console.log("newPersona en persona-sidebar");
        console.log("se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person",{}));
    }

}

customElements.define("persona-sidebar",PersonaSidebar);