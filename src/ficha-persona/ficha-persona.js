import {LitElement, html} from 'lit';

class FichaPersona extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}
        }
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
        this.name = "Manolo";
        this.yearsInCompany = 12;
        this.personInfo = this.updatePersonInfo();

    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){
        changedProperties.forEach(
            (oldValue, propName) => {
                console.log(`Propiedad ${propName} ha cambiado de valor, el valor antiguo era ${oldValue}`);
            }
        );
        
        //cuando la propiedad name cambia de valor
        if (changedProperties.has("name")){
            console.log(`Propiedad name ha cambiado de valor, el valor antiguo era ${changedProperties.get("name")} y el nuevo es ${this.name}`);
        }
        if (changedProperties.has("yearsInCompany")){
            console.log(`Propiedad yearsInCompany ha cambiado de valor, el valor antiguo era ${changedProperties.get("yearsInCompany")} y el nuevo es ${this.yearsInCompany}`);
            this.updatePersonInfo()
        }
    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `;
    }

    //funcion que se llama desde la función render() al @change el nombre en el formulario
    updateName(e){
        console.log("updateName");
        //console.log(e);
        this.name = e.target.value;
    }
    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        //console.log(e);
        this.yearsInCompany = e.target.value;
        // Es mejor ponerlo en updated() ya que así se updatea directamente y mantenemos el código más limpio y facil de mantener
        //this.personInfo = this.calcularInfo();
    }

    updatePersonInfo(){
        if(this.yearsInCompany >= 7){
            this.personInfo = "Lead";
        } else if(this.yearsInCompany >= 5){
            this.personInfo = "Senior";
        } else if(this.yearsInCompany >= 3){
            this.personInfo = "Team";
        } else{
            this.personInfo = "Junior";
        }
    }
}

customElements.define("ficha-persona",FichaPersona);