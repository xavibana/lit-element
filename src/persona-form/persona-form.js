import {LitElement, html} from 'lit';

class PersonaForm extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            person: {type: Object},
            editingPerson:{type: Boolean}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
        this.resetFormData();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre completo</label>
                    <input @input="${this.updateName}" type="text" class="form-control" placeholder="Nombre Completo"
                    .value="${this.person.name}"
                    ?disabled="${this.editingPerson}"/>
                </div>
                <div class="form-group">
                    <label>Perfil</label>
                    <textarea @input="${this.updateProfile}" class="form-control" placeholder="Perfil" rows="5"
                    .value="${this.person.profile}"></textarea>
                </div>
                <div class="form-group">
                    <label>Años en la empresa</label>
                    <input @input="${this.updateYearsInCompany}" type="text" class="form-control" placeholder="Años en la empresa"
                    .value="${this.person.yearsInCompany}"/>
                </div>
                <button @click="${this.goBack}" class="btn btn-primary"><strong>Cancelar</strong></button>
                <button @click="${this.storePerson}"class="btn btn-success"><strong>Guardar</strong></button>
            </form>
        </div>
        `;
    }

    goBack(e){
        console.log("goBack");
        console.log("se ha pulsado el botón de atras persona-form");
        e.preventDefault();

        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        this.resetFormData();
    }

    storePerson(e){
        console.log("storePerson");

        e.preventDefault();
        console.log("la propiedad name vale: "+this.person.name);
        console.log("la propiedad profile vale: "+this.person.profile);
        console.log("la propiedad yearsInCompany vale: "+this.person.yearsInCompany);
        this.person.photo ={
            src: "./img/persona.gif",
            alt: this.person.name
        };

        this.dispatchEvent(new CustomEvent("persona-form-store",{
            detail:{
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }))
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile con el valor " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad YearsInCompany con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    resetFormData(){
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;
    }



}

customElements.define("persona-form",PersonaForm);