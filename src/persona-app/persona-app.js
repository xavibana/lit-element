import {LitElement, html} from 'lit';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            people: {type: Array}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){
        console.log("updated en persona-app");

        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-app");

            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h1>Persona App</h1>
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
                <persona-main class="col-10" @people-updated="${this.peopleUpdated}"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.peopleStatsUpdated}"></persona-stats>
            <persona-main-dm @dataManagerPeople="${this.dataManagerPeople}"></persona-main-dm>
        `;
    }

    newPerson(e){
        console.log("newPerson en persona-app");
        
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    peopleStatsUpdated(e){
        console.log("peopleStatsUpdated en persona-app");
        console.log(e.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }

    peopleUpdated(e){
        console.log("peopleUpdated en persona-app");

        this.people = e.detail.people;

    }

    dataManagerPeople(e){
        console.log("dataManagerPeople en persona-App");
        console.log(e.detail);

        this.shadowRoot.querySelector("persona-main").people = e.detail.personas;

    }


}

customElements.define("persona-app",PersonaApp);