import {LitElement, html} from 'lit';

class EmisorEvento extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <h3>Emisor Evento</h3>
            <button @click="${this.sendEvent}">No pulsar nunca aquí</button>
        `;
    }

    sendEvent(e){
        console.log("Pulsando el botón");
        console.log(e);

        //envia el evento al Dispatcher para que le llegue al componente padre
        this.dispatchEvent(
            new CustomEvent(
                "test-event",{
                    "detail": {
                        "course" : "TechU",
                        "year"   : "2022" 
                    }
                }
            )   
        );
    }


}

customElements.define("emisor-evento",EmisorEvento);