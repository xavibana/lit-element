import {LitElement, html} from 'lit';

class ReceptorEvento extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            course: {type: String},
            year: {type: String}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <h3>Receptor Evento</h3>
            <h5>Este curso es de ${this.course} y estamos en el año ${this.year}</h5>
        `;
    }


}

customElements.define("receptor-evento",ReceptorEvento);