import {LitElement, html} from 'lit';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js'

class PersonaMain extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
        this.people = [];
/*        this.people = [
            {
                name: "Manolo",
                yearsInCompany: 10,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Manolo"
                },
                profile: "Jefe de equipo"
            }, {            
                name: "Josefa",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Josefa"
                }
                ,
                profile: "Programador Junior"
            }, {
                name: "MariTere",
                yearsInCompany: 5,
                photo: {
                    src: "./img/persona.gif",
                    alt: "MariTere"
                },
                profile: "Programador Senior"
            }, {
                name: "Ataulfo",
                yearsInCompany: 8,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Ataulfo"
                },
                profile: "Analista"
            }, {
                name: "Joshua",
                yearsInCompany: 7,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Joshua"
                },
                profile: "Analista/Programador"
            }, {
                name: "Estefany",
                yearsInCompany: 1,
                photo: {
                    src: "./img/persona.gif",
                    alt: "Estefany"
                },
                profile: "Programador Junior"
            }
        ]; */
        this.showPersonForm = false;
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){
        console.log("updated en persona-main");

        if(changedProperties.has("showPersonForm")){
            console.log("ha cambiado el valor de la propiedad showPersonForm en persona-main");
            (this.showPersonForm === true) ? this.showPersonFormData() : this.showPersonList();
        }
        if(changedProperties.has("people")){
            console.log("ha cambiado el valor de la propiedad PEOPLE en persona-main");

            this.dispatchEvent(
                new CustomEvent(
                    "people-updated",
                    {
                        detail: {
                            people: this.people
                          }
                    }
                )
            )
        }
    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>        
            <div class="row" id="peopleList">
                <main class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado 
                        fname="${person.name}"
                        fYearsInCompany="${person.yearsInCompany}"
                        .fPhoto="${person.photo}"
                        fProfile="${person.profile}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                        >
                        </persona-ficha-listado>`
                        
                    )}
                </main>
            </div>
            <div class="row">
                <persona-form 
                @persona-form-close="${this.personFormClose}"
                @persona-form-store="${this.personaFormStore}" 
                id="personaForm" class="d-none border rounded border-primary"></persona-form>
            </div>
        `;
    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e){
        console.log("infoPerson en persona-main");
        console.log("Se ha pedido más info de la persona "+ e.detail.name);
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personaForm").person = person;
        this.shadowRoot.getElementById("personaForm").editingPerson = true;
        this.showPersonForm = true;
    }


    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personaForm").classList.add("d-none");

    }

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando el formulario de personas");

        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personaForm").classList.remove("d-none");

    }

    personFormClose(e){
        console.log("personFormClose de persona-main");

        this.showPersonForm =false;
    }

    personaFormStore(e){
        console.log("personaFormStore de persona-main");
        console.log(e.detail.person);

        if(e.detail.editingPerson === true){
            console.log("se va a actualizar la persona con nombre: "+ e.detail.person.name);
            this.people = this.people.map(
                person => (person.name === e.detail.person.name) ? person = e.detail.person : person
            );
            // Tenemos que modificar el array entero y no sólo una posición cómo hacíamos ya que sinó lit no detecta el cambio en el método Update()
            /**let indexPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );

            if(indexPerson >= 0){
                console.log("Persona encontrada");
                this.people[indexPerson] = e.detail.person;
            }
            **/

        }else{
            console.log("Se va a almacenar a una persona nueva");
            //añadimos en la última posición del array a la nueva persona con Spread Syntax
            this.people = [...this.people,e.detail.person];
            //this.people.push(e.detail.person);
        }

        console.log("Persona almacenada");

        this.showPersonForm = false;
    }

}

customElements.define("persona-main",PersonaMain);

// Aserción
// mocha.AssertEquals()calculadora.suma(2,2), 4);
// Invariante = 4 (pasado en la función Assert)

// Actual VS Expected y Fluent Interfaces
// mocha.should.be.equal(4).calculadora.suma(2,2);

// Ejemplo Mocks
// Conversor Moneda
// calculator = XXnewCalculatorXX -> calculatorMock <- (herramienta de Mocks) Se llama a un código 
// currencyConverter.convert("USD","EUR", calculator.mult(2, EUR.value))