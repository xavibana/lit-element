import {LitElement, html, css} from 'lit';

class TestBootstrap extends LitElement{


    //propiedades del componente
    static get properties(){
        return{
        };
    }

    //propiedades de stylo del componente
    //este CSS en concreto reinicializa todos los estilos para los que tiene por defecto el navegador.
    /*static get styles(){
        return css`
            :host{
                all: initial
            }
        `;
    }*/

    static get styles(){
        return css`
            .redbg{
                background-color: red;
            }
            .greenbg{
                background-color: green;
            }
            .bluebg{
                background-color: blue;
            }
            .greybg{
                background-color: grey;
            }
        `;
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }



    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h1>Test Boostrap</h1>
            <div class="row greybgtienes el import de Css?">
                <div class="col redbg">col 1</div>
                <div class="col greenbg">col 2</div>
                <div class="col bluebg">col 3</div>
            </div>
        `;
    }


}

customElements.define("test-bootstrap",TestBootstrap);