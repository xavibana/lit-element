import {LitElement, html} from 'lit';

class TestApi extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            movies: {type: Array}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
        this.movies= [];
        this.getMovieData();
    }

    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }

    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            ${this.movies.map(
                movie => html`
                <div>La película ${movie.title} fue dirigida por ${movie.director}</div>
                `
            )}
        `;
    }

    getMovieData(){
        console.log("getMovieData");
        console.log("obteniendo los datos de las películas");

        let xhr = new XMLHttpRequest();

        //Asíncrono con CallBack
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Llamada completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                this.movies = APIResponse.results;
            }
        }
        xhr.open("GET","https://swapi.dev/api/films");
        xhr.send();
        console.log("Fin de getMovieData");
    }


}

customElements.define("test-api",TestApi);