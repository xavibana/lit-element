import {LitElement, html} from 'lit';

class PersonaFichaListado extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
            fname: {type: String},
            fYearsInCompany: {type: Number},
            fPhoto: {type: Object},
            fProfile: {type: String}
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            
            <div class="card h-100">
                <img src="${this.fPhoto.src}" height="auto" width="auto" alt="${this.fPhoto.alt}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${this.fname}</h5>
                    <p class="card-text">${this.fProfile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.fYearsInCompany} años en la empresa</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button @click="${this.deletePerson}" class="btn btn-danger col-5"><Strong>X</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"><strong>Info</strong></button>
                </div>
            </div>
        
        `;
           /* <div>
                <label>Nombre</label>
                <input type="text" value="${this.fname}"/>
                <br />
                <label>Años en la compañía</label>
                <input type="text" value="${this.fYearsInCompany}"/>
                <br />
                <img src="${this.fPhoto.src}" height="200" width="400" alt="${this.fPhoto.alt}"> 
                <br />
            </div>*/
        
    }

    deletePerson(e){
        console.log("deletePerson");
        console.log("se va a borrar la persona de nombre "+ this.fname);
        
        //creamos el custom event para mandárselo a la web component gestora
        this.dispatchEvent(
            new CustomEvent(
                "delete-person", {
                    detail : {
                        name: this.fname
                    }
                }
            )
        );
    }

    moreInfo(e){
        console.log("moreInfo");
        console.log("se ha pedido más información de la persona "+ this.fname);

        this.dispatchEvent(
            new CustomEvent(
                "info-person",
                {
                    detail: {
                        name: this.fname
                    }
                }
            )
        )
    }


}

customElements.define("persona-ficha-listado",PersonaFichaListado);