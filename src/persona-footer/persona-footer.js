import {LitElement, html} from 'lit';

class PersonaFooter extends LitElement{


    //datos de los input del formulario
    static get properties(){
        return{
        };
    }

    //hay que llamar a super para llamar al contructor de la clase LitElement
    //aquí se relaciona el valor del get properties
    constructor(){
        super();
    }


    //Lo que se realiza tras los eventos y actualizaciones en la página web
    updated(changedProperties){

    }


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <h5>Persona Footer</h5>
        `;
    }


}

customElements.define("persona-footer",PersonaFooter);