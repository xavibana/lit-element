import {LitElement, html} from 'lit';

class HolaMundo extends LitElement{


    // los componentes tiene que "devolver" una vista que se hace en la funcion render
    render(){
        return html`
            <div>Hola mundo!</div>
        `;
    }
}

customElements.define("hola-mundo",HolaMundo);